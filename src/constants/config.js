const dotenv = require('dotenv');
dotenv.config();

const MONGO_DB_HOST = process.env.MONGO_DB_HOST || 'mongo'
const MONGO_DB_PORT = process.env.MONGO_DB_PORT || 27017
const MONGO_DB = process.env.MONGO_DB

const MONGODB_URI = process.env.MONGODB_URI || `mongodb://${MONGO_DB_HOST}:${MONGO_DB_PORT}/${MONGO_DB}`

const APP_PORT = (process.env.APP_PORT || 80)


module.exports = {
	MONGODB_URI,
	APP_PORT
}

