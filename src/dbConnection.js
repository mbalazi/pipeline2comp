
const constants = require('./constants/config');
const mongoose = require('mongoose');

mongoose.pluralize(null);

console.log(process.env.MONGODB_URI || constants.MONGODB_URI)
mongoose.connect(process.env.MONGODB_URI || constants.MONGODB_URI, {
  useCreateIndex: true,
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false
});

module.exports = {

};
