
const http = require('http');
const app = require('./app');

const constants = require('./constants/config');
const port = constants.APP_PORT || 80

const server = http.createServer(app);

server.listen(port, function () {
        console.log(`App listening on port ${port}!`);
});