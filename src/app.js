
const express = require('express');
const app = express();

const cors = require('cors')
app.use(cors());

const bodyParser = require('body-parser');
app.use(bodyParser.json());

const session = require('express-session');
var memoryStore = new session.MemoryStore();

const Keycloak = require('keycloak-connect');
var keycloak = new Keycloak({ store: memoryStore });

app.use(session({
    secret: 'BeALongSecret',
    resave: false,
    saveUninitialized: true,
    store: memoryStore
}));
app.use(keycloak.middleware());

const ping = require('./v1.0/routes/ping');
app.use('/', ping);
app.use(keycloak.middleware({ logout: '/' }));

module.exports = app;
