
const express = require('express');
const router = express.Router();

router.get('/ping', ping);

module.exports = router;

function ping(req, res, next) {
    console.log('collaboration api pinged')
    res.status(401).send('PONG');
}